from collections import defaultdict
from copy import deepcopy
import seaborn as sns
import matplotlib.pyplot as plt
import pandas
import pandas as pd
from sklearn.metrics import r2_score
import os
import pickle
from sklearn.cluster import KMeans
from sklearn.ensemble import RandomForestRegressor
from sklearn.datasets import make_regression
from sklearn.metrics import mean_squared_error
import numpy as np


class PDSRF():
    def __init__(self,
                 n_clusters=100,
                 n_estimators=1000,
                 max_depth=10,
                 threshold=0):
        self.threshold = threshold
        self.n_clusters = n_clusters
        self.n_estimators = n_estimators
        self.max_depth = max_depth
        self.weights = {}
        self.grouped_trees = {}
        self.random_state = 0

    def fit_forest(self, x, y, **kwargs):
        self.n_clusters = kwargs.pop('n_clusters', self.n_clusters)
        regr = RandomForestRegressor(max_depth=kwargs.pop('max_depth',
                                                          self.max_depth),
                                     random_state=kwargs.pop('random_state',
                                                             self.random_state),
                                     n_estimators=kwargs.pop('n_estimators',
                                                             self.n_estimators),
                                     **kwargs)
        regr.fit(x, y)
        terminals = regr.apply(x)
        n_trees = terminals.shape[1]
        a = terminals[:, 0]
        prox_mat = 1 * np.equal.outer(a, a)
        for i in range(1, n_trees):
            a = terminals[:, i]
            prox_mat += 1 * np.equal.outer(a, a)
        prox_mat = prox_mat / n_trees

        self._clusterize(self.n_clusters,
                         prox_mat,
                         regr
                         )

    def _clusterize(self, n_clusters, prox_mat, regr):
        neigh = KMeans(n_clusters=n_clusters)
        res = neigh.fit_predict(prox_mat)
        grouped = defaultdict(lambda: [])
        for i in range(len(res)):
            grouped[res[i]].append(regr.estimators_[i])
        trees = []
        for k, v in grouped.items():
            tree = RandomForestRegressor()
            tree.n_estimators = len(v)
            tree.estimators_ = v
            tree.max_depth = regr.max_depth
            tree.n_features_in_ = regr.n_features_
            tree.n_outputs_ = regr.random_state
            trees.append(tree)
        self.grouped_trees = {i: trees[i] for i in range(len(trees))}

    # def clusterize(self, n_clusters, estimators):
    #     dct = {tuple(i.feature_importances_): i for i in
    #            estimators}
    #     neigh = KMeans(n_clusters=n_clusters)
    #     res = neigh.fit_predict(list(dct.keys()))
    #     grouped = defaultdict(lambda: [])
    #     for i in range(len(res)):
    #         grouped[res[i]].append(estimators[i])
    #     trees = []
    #     for k, v in grouped.items():
    #         tree = RandomForestRegressor()
    #         tree.n_estimators = len(v)
    #         tree.estimators_ = v
    #         trees.append(tree)
    #     self.grouped_trees = trees
    #     self.n_clasters = n_clusters

    def count_weights(self, x, y, threshold=None):
        threshold = threshold if threshold else self.threshold
        weights = {}
        for i in range(self.n_clusters):
            pred = self.grouped_trees[i].predict(x)
            metric = r2_score(y,pred)
            if metric > threshold:
                weights[i] = metric
        sum_weights = sum(weights.values())
        self.weights = {k: v / sum_weights for k, v in weights.items()}

    def predict(self, X):
        return sum(self.grouped_trees[i].predict(X) * self.weights[i]
                   for i in range(self.n_clusters))

    def dump_model(self, path):
        for index, tree in self.grouped_trees.items():
            pickle.dump(tree, open(path+f'tree_{index}.pickle', 'wb'))

    def load_model(self, path, indexes=None):
        files = filter(lambda x: x.startswith('tree'), os.listdir(path))
        if indexes:
            files = filter(lambda x:
                           int(x.split('_')[1].split('.')[0]) in indexes,
                           files)
        grouped_trees = {}
        for filename in files:
            index = int(filename.split('_')[1].split('.')[0])
            grouped_trees[index] = pickle.load(open(filename, 'rb'))
        self.grouped_trees = grouped_trees

def load_data(path):
    df = pd.read_csv(path)
    return df


def hampel(df: pandas.DataFrame):
    for column in df.columns:
        print(column)
        if column != 'Datetime' and column != 'is_weekend':
            vals = df[column]
            difference = np.abs(vals.median()-vals)
            median_abs_deviation = difference.median()
            threshold = 3 * median_abs_deviation
            outlier_idx = difference > threshold
            vals[outlier_idx] = np.nan
            df[column] = vals
    return df


def prepare_data(df: pandas.DataFrame):
    filtered_df = hampel(df)
    interpolated_df = filtered_df.interpolate()
    y = interpolated_df['kWh']
    res_df = interpolated_df.drop(columns=['kWh', 'Datetime'])
    return res_df[1:1000], y[1:1000]


def plot_values(time, y, y_pred):
    time = pd.to_datetime(time)
    dct = {'time': time.values, 'y': y.values, 'y_pred': y_pred.values}
    df = pd.DataFrame(dct)
    # plt.plot(df.index.values, df['y'])
    # plt.show()
    print(df)
    df.plot(x='time')


def main():
    path = 'weather-energy-data-update.csv'
    df = load_data(path)
    dt = df['Datetime'][1:1000]
    x, y = prepare_data(df)
    clf = PDSRF(n_clusters=10)
    clf.fit_forest(x, y)

    clf.count_weights(x, y, 0.5)
    y_pred = pd.Series(clf.predict(x), index=y.index)
    plot_values(dt, y, y_pred)
    plt.show()


if __name__ == '__main__':
    main()